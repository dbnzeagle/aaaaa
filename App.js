import React from 'react';
import {StyleSheet, Button, Text, View} from 'react-native';
import {createStackNavigator, createAppContainer, createBottomTabNavigator} from 'react-navigation'

class HomeScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>Виталя Дарова</Text>
                <Button
                    title="Go to details screen"
                    onPress={() => this.props.navigation.navigate('Details')}
                />
            </View>
        );
    }
}

class DetailsScreen extends React.Component {
    render() {
        return (
            <View style={{flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: "grey"}}>
                <Text>Details Screen</Text>
            </View>
        );
    }
}

class SettingsScreen extends React.Component {
    render() {
        return (
            <View style={{flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: "grey"}}>
                <Text>Details Screen</Text>
                <Button
                    title="Go to profile screen"
                    onPress={() => this.props.navigation.navigate('Profile')}
                />

            </View>
        );
    }
}

class ProfileScreen extends React.Component {
    render() {
        return (
            <View style={{flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: "grey"}}>
                <Text>Profile Screen</Text>


            </View>
        );
    }
}



const HomeStack = createStackNavigator({
    Home: HomeScreen,
    Details: DetailsScreen
});

const SettingsStack = createStackNavigator({
    Settings: SettingsScreen,
    Profile: ProfileScreen
});

const TabNavigator = createBottomTabNavigator({
    Home: HomeStack,
    Settings: SettingsStack
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'grey',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
export default createAppContainer(TabNavigator);

